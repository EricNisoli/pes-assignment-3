CREATE USER IF NOT EXISTS 'skyscanner'@'%' IDENTIFIED BY 'skypilot';
CREATE USER IF NOT EXISTS 'skyscanner_test'@'%' IDENTIFIED BY 'skypilot_test';

GRANT ALL PRIVILEGES ON skyscanner.* TO 'skyscanner'@'%';
GRANT ALL PRIVILEGES ON skyscanner_test.* TO 'skyscanner_test'@'%';
