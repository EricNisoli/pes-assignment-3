import com.assignment3.daos.ClientDao;
import com.assignment3.entities.Client;
import com.assignment3.entities.Travel;
import com.assignment3.utils.SessionHelper;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public final class ClientDaoTestCase extends AbstractDaoTestCase<String, Client, ClientDao>
{

    private final static int TRAVELS_COUNT = 5;
    private final static String PSW = "PASSWORD";

    @Test
    protected void findByUsername()
    {
        Client client = Utils.createClient();
        client.setUsername("ericniso");
        baseDao().create(client);

        SessionHelper.getEntityManager().flush();
        SessionHelper.getEntityManager().clear();

        List<Client> clients = baseDao().findByUsername("Eric");
        Assertions.assertEquals(1, clients.size());
    }

    @Test
    protected void orderByUsernameAsc()
    {
        Client client1 = Utils.createClient();
        client1.setUsername("ZZ");
        Client client2 = Utils.createClient();
        client2.setUsername("AA");

        baseDao().create(client1);
        baseDao().create(client2);

        SessionHelper.getEntityManager().flush();
        SessionHelper.getEntityManager().clear();

        List<Client> clients = baseDao().orderByUsername(true);
        Assertions.assertEquals("AA", clients.get(0).getUsername());
    }

    @Test
    protected void orderByUsernameDesc()
    {
        Client client1 = Utils.createClient();
        client1.setUsername("AAA");
        Client client2 = Utils.createClient();
        client2.setUsername("ZZZ");

        baseDao().create(client1);
        baseDao().create(client2);

        SessionHelper.getEntityManager().flush();
        SessionHelper.getEntityManager().clear();

        List<Client> clients = baseDao().orderByUsername(false);
        Assertions.assertEquals("ZZZ", clients.get(0).getUsername());
    }

    @Test
    protected void addTravels()
    {
        Client client = Utils.createClient();
        for (int i = 0; i < TRAVELS_COUNT; i++)
        {
            client.getTravels().add(Utils.createTravel());
        }
        baseDao().create(client);

        SessionHelper.getEntityManager().flush();
        SessionHelper.getEntityManager().clear();

        client = baseDao().find(client.getId());
        Assertions.assertFalse(persistenceUnitUtil().isLoaded(client, "travels"));
        Assertions.assertEquals(TRAVELS_COUNT, client.getTravels().size());
        Assertions.assertTrue(persistenceUnitUtil().isLoaded(client, "travels"));
    }

    @Test
    protected void deleteTravels()
    {
        Client client = Utils.createClient();
        for (int i = 0; i < TRAVELS_COUNT; i++)
        {
            client.getTravels().add(Utils.createTravel());
        }
        baseDao().create(client);

        SessionHelper.getEntityManager().flush();
        SessionHelper.getEntityManager().clear();

        client = baseDao().find(client.getId());
        Assertions.assertEquals(TRAVELS_COUNT, client.getTravels().size());

        client.getTravels().clear();
        baseDao().update(client);

        SessionHelper.getEntityManager().flush();
        SessionHelper.getEntityManager().clear();

        client = baseDao().find(client.getId());
        Assertions.assertEquals(0, client.getTravels().size());
    }

    @Test
    protected void addSingleTravel()
    {
        Client client = Utils.createClient();
        for (int i = 0; i < TRAVELS_COUNT; i++)
        {
            client.getTravels().add(Utils.createTravel());
        }
        baseDao().create(client);

        SessionHelper.getEntityManager().flush();
        SessionHelper.getEntityManager().clear();

        client = baseDao().find(client.getId());
        Assertions.assertEquals(TRAVELS_COUNT, client.getTravels().size());

        int previous_count = client.getTravels().size();

        client.getTravels().add(Utils.createTravel());
        baseDao().update(client);

        SessionHelper.getEntityManager().flush();
        SessionHelper.getEntityManager().clear();

        client = baseDao().find(client.getId());
        Assertions.assertEquals(previous_count + 1, client.getTravels().size());
    }

    @Test
    protected void deleteSingleTravel()
    {
        Client client = Utils.createClient();
        for (int i = 0; i < TRAVELS_COUNT; i++)
        {
            client.getTravels().add(Utils.createTravel());
        }

        Travel travel = Utils.createTravel();
        client.getTravels().add(travel);
        baseDao().create(client);

        SessionHelper.getEntityManager().flush();

        client = baseDao().find(client.getId());
        Assertions.assertEquals(TRAVELS_COUNT + 1, client.getTravels().size());

        int previous_count = client.getTravels().size();

        client.getTravels().remove(travel);
        baseDao().update(client);

        SessionHelper.getEntityManager().flush();
        SessionHelper.getEntityManager().clear();

        client = baseDao().find(client.getId());
        Assertions.assertEquals(previous_count - 1, client.getTravels().size());
    }

    @Override
    protected void afterCreate(Client instance)
    {
        super.afterCreate(instance);
        Assertions.assertNotEquals(instance.getPassword(), PSW);
        Assertions.assertTrue(instance.isPasswordMatch(PSW));
    }

    @Override
    protected void afterUpdate(Client old, Client updated)
    {
        super.afterUpdate(old, updated);
        Assertions.assertNotEquals(old.getUsername(), updated.getUsername());
        Assertions.assertNotEquals(old.getPassword(), updated.getPassword());
        Assertions.assertNotEquals(old.getName(), updated.getName());
        Assertions.assertNotEquals(old.getSurname(), updated.getSurname());
        Assertions.assertNotEquals(old.getBirthday(), updated.getBirthday());
    }

    @Override
    protected ClientDao createDao()
    {
        return new ClientDao();
    }

    @Override
    protected Client getEntity()
    {

        try
        {
            Client client = Utils.createClient();
            client.setPassword(PSW);
            client.setBirthday(new SimpleDateFormat("yyyy-MM-dd")
                    .parse("1996-27-06"));

            return client;

        } catch (ParseException e)
        {
            e.printStackTrace();
        }

        return null;
    }

    @Override
    protected Client getUpdatedEntity(Client instance)
    {
        instance.setUsername(String.format("%s-%s", instance.getUsername(), "Updated"));
        instance.setPassword(String.format("%s-%s", instance.getPassword(), "Updated"));
        instance.setName(String.format("%s-%s", instance.getName(), "Updated"));
        instance.setSurname(String.format("%s-%s", instance.getSurname(), "Updated"));

        Calendar c = Calendar.getInstance();
        c.setTime(instance.getBirthday());
        c.add(Calendar.MINUTE, 1);

        instance.setBirthday(c.getTime());
        return instance;
    }

    @Override
    protected Client getInvalidDomainEntity()
    {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(new Date());
        calendar.add(Calendar.MONTH, 1);

        Client client = Utils.createClient();
        client.setBirthday(calendar.getTime());

        return client;
    }

    @Override
    protected Client getInvalidInterDomainEntity()
    {
        return null;
    }

    @Override
    protected Client getInvalidDomainUpdatedEntity(Client instance)
    {
        return null;
    }

    @Override
    protected Client getInvalidInterDomainUpdatedEntity(Client instance)
    {
        return null;
    }


    @Override
    protected Client copyEntityInstance(Client instance)
    {
        return Utils.clone(false, instance);
    }
}
