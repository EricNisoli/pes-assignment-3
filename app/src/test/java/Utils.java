import com.assignment3.entities.*;

import java.util.Date;

public final class Utils
{
    private static int counter = -1;

    public static int incrementCounter()
    {
        counter++;
        return counter;
    }

    public static Client createClient()
    {
        return new Client(String.format("Test-%d", Utils.incrementCounter()), "Test", "Test", "Test", new Date());
    }

    public static Company createCompany()
    {
        return new Company(String.format("Test-%d", Utils.incrementCounter()), "Test", "Test", "Test");
    }

    public static Airplane createAirplane()
    {
        return new Airplane("Test", "Test");
    }

    public static Airport createAirport()
    {
        return new Airport(String.format("Test-%d", Utils.incrementCounter()), "Test", "Test", "Test");
    }

    public static Travel createTravel()
    {
        return new Travel(new Date(), Travel.TravelStatus.PLANNED);
    }

    public static Flight createFlight()
    {
        return new Flight(String.format("Test-%d", Utils.incrementCounter()), new Date(), new Date());
    }

    public static Pilot createPilot()
    {
        return new Pilot("Test", "Test", new Date(), "Test");
    }

    public static Assistant createAssistant()
    {
        return new Assistant("Test", "Test", new Date(), "Test");
    }

    public static Airplane clone(boolean deep, Airplane instance)
    {
        Airplane copy = new Airplane();
        copy.setId(instance.getId());
        copy.setModel(instance.getModel());
        copy.setManufacturer(instance.getManufacturer());

        if (deep)
        {
            // TODO: copy collections
        }

        return copy;
    }

    public static Airport clone(boolean deep, Airport instance)
    {
        Airport copy = new Airport();
        copy.setId(instance.getId());
        copy.setName(instance.getName());
        copy.setCity(instance.getCity());
        copy.setState(instance.getState());

        if (deep)
        {
            // TODO: copy collections
        }

        return copy;
    }

    public static Assistant clone(boolean deep, Assistant instance)
    {
        Assistant copy = new Assistant();
        copy.setId(instance.getId());
        copy.setName(instance.getName());
        copy.setSurname(instance.getSurname());
        copy.setBirthday(instance.getBirthday());
        copy.setLanguage(instance.getLanguage());

        if (deep)
        {
            // TODO: copy collections
        }

        return copy;
    }

    public static Client clone(boolean deep, Client instance)
    {
        Client copy = new Client();
        copy.setId(instance.getId());
        copy.setUsername(instance.getUsername());
        copy.setPassword(instance.getPassword());
        copy.setName(instance.getName());
        copy.setSurname(instance.getSurname());
        copy.setBirthday(instance.getBirthday());

        if (deep)
        {
            // TODO: copy collections
        }

        return copy;
    }

    public static Company clone(boolean deep, Company instance)
    {
        Company copy = new Company();
        copy.setId(instance.getId());
        copy.setUsername(instance.getUsername());
        copy.setPassword(instance.getPassword());
        copy.setName(instance.getName());
        copy.setVAT(instance.getVAT());

        if (deep)
        {
            // TODO: copy collections
        }

        return copy;
    }

    public static Flight clone(boolean deep, Flight instance)
    {
        Flight copy = new Flight();
        copy.setId(instance.getId());
        copy.setDepartureTime(instance.getDepartureTime());
        copy.setArrivalTime(instance.getArrivalTime());

        if (deep)
        {
            // TODO: copy collections
        }

        return copy;
    }

    public static Pilot clone(boolean deep, Pilot instance)
    {
        Pilot copy = new Pilot();
        copy.setId(instance.getId());
        copy.setName(instance.getName());
        copy.setSurname(instance.getSurname());
        copy.setBirthday(instance.getBirthday());
        copy.setGrade(instance.getGrade());

        if (deep)
        {
            // TODO: copy collections
        }

        return copy;
    }

    public static Travel clone(boolean deep, Travel instance)
    {
        Travel copy = new Travel();
        copy.setId(instance.getId());
        copy.setDate(instance.getDate());
        copy.setStatus(instance.getStatus());

        if (deep)
        {
            // TODO: copy collections
        }

        return copy;
    }
}
