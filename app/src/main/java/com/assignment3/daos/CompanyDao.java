package com.assignment3.daos;

import com.assignment3.entities.Company;
import com.assignment3.entities.Travel;
import com.assignment3.utils.Pair;

import java.util.List;

public class CompanyDao extends AbstractDao<Company>
{

    public List<Company> findByVAT(String vat)
    {
        return findByLike("VAT", vat);
    }

    public List<Company> orderByVAT(boolean ascending)
    {
        return orderBy("VAT", ascending);
    }

    @Override
    protected void updateRelationships(Company instance)
    {
        for (Travel t : instance.getTravels())
        {
            t.setCompany(instance);
        }
    }

    @Override
    protected Pair<Boolean, String> checkDomain(Company instance)
    {
        return Pair.from(true, null);
    }

    @Override
    protected Pair<Boolean, String> checkInterDomain(Company instance)
    {
        return Pair.from(true, null);
    }

    @Override
    protected Class<Company> getGenericClassName()
    {
        return Company.class;
    }
}
