package com.assignment3.daos;

import com.assignment3.entities.Airport;
import com.assignment3.entities.Flight;
import com.assignment3.utils.Pair;

import java.util.List;

public class AirportDao extends AbstractDao<Airport>
{

    public List<Airport> findByName(String  name)
    {
        return findByLike("name", name);
    }

    public List<Airport> orderByName(boolean ascending)
    {
        return orderBy("name", ascending);
    }

    public List<Airport> findByNameOrCity(String name, String city)
    {
        QueryPreparation<Airport, Airport> qp = createQueryPreparation();

        return findByMultipleExp(QueryConcatType.OR, qp,
                qp.builder.like(qp.root.get("name"), String.format("%%%s%%", name)),
                qp.builder.like(qp.root.get("city"), String.format("%%%s%%", city)));
    }

    @Override
    protected void updateRelationships(Airport instance)
    {
        for (Flight f : instance.getDepartures())
        {
            f.setDeparture(instance);
        }

        for (Flight f : instance.getArrivals())
        {
            f.setArrival(instance);
        }
    }

    @Override
    protected Pair<Boolean, String> checkDomain(Airport instance)
    {
        return Pair.from(true, null);
    }

    @Override
    protected Pair<Boolean, String> checkInterDomain(Airport instance)
    {
        return Pair.from(true, null);
    }

    @Override
    protected Class<Airport> getGenericClassName()
    {
        return Airport.class;
    }
}
