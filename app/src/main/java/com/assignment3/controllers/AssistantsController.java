package com.assignment3.controllers;

import com.assignment3.entities.Assistant;
import com.assignment3.proxies.AssistantProxy;
import com.assignment3.services.AssistantService;
import com.assignment3.services.AbstractService;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class AssistantsController extends ControllerBase<Assistant, AssistantProxy>
{
    @Override
    protected AbstractService<Assistant> createService()
    {
        return new AssistantService();
    }

    @Override
    protected Serializable convertId(String id)
    {
        return id;
    }

    @Override
    protected Assistant getEmptyModel()
    {
        return new Assistant();
    }

    @Override
    protected AssistantProxy createProxy(Assistant instance)
    {
        return new AssistantProxy(instance);
    }

    @Override
    protected List<AssistantProxy> createProxy(List<Assistant> instance)
    {
        List<AssistantProxy> list = new ArrayList<>();

        for (Assistant i : instance)
        {
            list.add(createProxy(i));
        }

        return list;
    }
}
