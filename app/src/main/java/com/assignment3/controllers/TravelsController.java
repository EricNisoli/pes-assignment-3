package com.assignment3.controllers;

import com.assignment3.entities.Travel;
import com.assignment3.proxies.TravelProxy;
import com.assignment3.services.AbstractService;
import com.assignment3.services.TravelService;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class TravelsController extends ControllerBase<Travel, TravelProxy>
{
    @Override
    protected AbstractService<Travel> createService()
    {
        return new TravelService();
    }

    @Override
    protected Serializable convertId(String id)
    {
        return id;
    }

    @Override
    protected Travel getEmptyModel()
    {
        return new Travel();
    }

    @Override
    protected TravelProxy createProxy(Travel instance)
    {
        return new TravelProxy(instance);
    }

    @Override
    protected List<TravelProxy> createProxy(List<Travel> instance)
    {
        List<TravelProxy> list = new ArrayList<>();

        for (Travel i : instance)
        {
            list.add(createProxy(i));
        }

        return list;
    }
}
