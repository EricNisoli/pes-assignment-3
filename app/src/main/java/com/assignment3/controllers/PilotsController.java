package com.assignment3.controllers;

import com.assignment3.entities.Pilot;
import com.assignment3.proxies.PilotProxy;
import com.assignment3.services.AbstractService;
import com.assignment3.services.PilotService;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class PilotsController extends ControllerBase<Pilot, PilotProxy>
{
    @Override
    protected AbstractService<Pilot> createService()
    {
        return new PilotService();
    }

    @Override
    protected Serializable convertId(String id)
    {
        return id;
    }

    @Override
    protected Pilot getEmptyModel()
    {
        return new Pilot();
    }

    @Override
    protected PilotProxy createProxy(Pilot instance)
    {
        return new PilotProxy(instance);
    }

    @Override
    protected List<PilotProxy> createProxy(List<Pilot> instance)
    {
        List<PilotProxy> list = new ArrayList<>();

        for (Pilot i : instance)
        {
            list.add(createProxy(i));
        }

        return list;
    }
}
