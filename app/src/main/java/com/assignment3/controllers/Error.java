package com.assignment3.controllers;

/**
 * Utility class needed to return errors as response
 */
public final class Error
{
    private String error;

    public Error(String error)
    {
        this.error = error;
    }

    public String getError()
    {
        return error;
    }

    public void setMessage(String error)
    {
        this.error = error;
    }
}
