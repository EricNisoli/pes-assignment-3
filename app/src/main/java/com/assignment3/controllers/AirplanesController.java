package com.assignment3.controllers;

import com.assignment3.entities.Airplane;
import com.assignment3.proxies.AirplaneProxy;
import com.assignment3.services.AbstractService;
import com.assignment3.services.AirplaneService;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class AirplanesController extends ControllerBase<Airplane, AirplaneProxy>
{
    @Override
    protected AbstractService<Airplane> createService()
    {
        return new AirplaneService();
    }

    @Override
    protected Serializable convertId(String id)
    {
        return id;
    }

    @Override
    protected Airplane getEmptyModel()
    {
        return new Airplane();
    }

    @Override
    protected AirplaneProxy createProxy(Airplane instance)
    {
        return new AirplaneProxy(instance);
    }

    @Override
    protected List<AirplaneProxy> createProxy(List<Airplane> instance)
    {
        List<AirplaneProxy> list = new ArrayList<>();

        for (Airplane i : instance)
        {
            list.add(createProxy(i));
        }

        return list;
    }
}
