package com.assignment3.controllers;

import com.assignment3.entities.Company;
import com.assignment3.proxies.CompanyProxy;
import com.assignment3.services.AbstractService;
import com.assignment3.services.CompanyService;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class CompaniesController extends ControllerBase<Company, CompanyProxy>
{
    @Override
    protected AbstractService<Company> createService()
    {
        return new CompanyService();
    }

    @Override
    protected Serializable convertId(String id)
    {
        return id;
    }

    @Override
    protected Company getEmptyModel()
    {
        return new Company();
    }

    @Override
    protected CompanyProxy createProxy(Company instance)
    {
        return new CompanyProxy(instance);
    }

    @Override
    protected List<CompanyProxy> createProxy(List<Company> instance)
    {
        List<CompanyProxy> list = new ArrayList<>();

        for (Company i : instance)
        {
            list.add(createProxy(i));
        }

        return list;
    }
}
