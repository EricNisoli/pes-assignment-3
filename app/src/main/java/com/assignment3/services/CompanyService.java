package com.assignment3.services;

import com.assignment3.daos.AbstractDao;
import com.assignment3.daos.CompanyDao;
import com.assignment3.entities.Company;

public class CompanyService extends AbstractService<Company>
{

    @Override
    protected AbstractDao<Company> getDao()
    {
        return new CompanyDao();
    }
}
