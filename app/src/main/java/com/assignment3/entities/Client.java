package com.assignment3.entities;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "Clients")
public class Client extends User implements Serializable
{

    @Column(name = "name", nullable = false)
    private String name;

    @Column(name = "surname", nullable = false)
    private String surname;

    @Column(name = "birthday", nullable = false)
    private Date birthday;

    @ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinTable(name = "Travels_has_Clients",
            joinColumns = { @JoinColumn(name = "client_id", referencedColumnName = "id") },
            inverseJoinColumns = { @JoinColumn(name = "travel_id", referencedColumnName = "id") })
    private Set<Travel> travels;

    public Client()
    {
        travels = new HashSet<>(0);
    }

    public Client(String username, String password, String name, String surname, Date birthday)
    {
        super(username, password);
        setName(name);
        setSurname(surname);
        setBirthday(birthday);
        travels = new HashSet<>(0);
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getSurname()
    {
        return surname;
    }

    public void setSurname(String surname)
    {
        this.surname = surname;
    }

    public Date getBirthday()
    {
        return birthday;
    }

    public void setBirthday(Date birthday)
    {
        this.birthday = birthday;
    }

    public Set<Travel> getTravels()
    {
        return travels;
    }
}
