package com.assignment3.entities;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "Companies")
public class Company extends User implements Serializable
{

    @Column(name = "name", nullable = false)
    private String name;

    @Column(name = "VAT", nullable = false)
    private String VAT;

    @OneToMany(mappedBy = "company", cascade = CascadeType.ALL, fetch = FetchType.LAZY, orphanRemoval = true)
    private Set<Travel> travels;

    public Company()
    {
        travels = new HashSet<>(0);
    }

    public Company(String username, String password, String name, String VAT)
    {
        super(username, password);
        setName(name);
        setVAT(VAT);
        travels = new HashSet<>(0);
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getVAT()
    {
        return VAT;
    }

    public void setVAT(String VAT)
    {
        this.VAT = VAT;
    }

    public Set<Travel> getTravels()
    {
        return travels;
    }
}
