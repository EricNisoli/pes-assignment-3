package com.assignment3.proxies;

import com.assignment3.entities.Company;
import com.assignment3.entities.Travel;

import java.util.ArrayList;
import java.util.List;

public class CompanyProxy extends AbstractProxy<Company>
{
    public CompanyProxy(Company instance)
    {
        super(instance);
    }

    public String getId()
    {
        return obtainInstance().getId();
    }

    public void setId(String id)
    {
        obtainInstance().setId(id);
    }

    public String getUsername()
    {
        return obtainInstance().getUsername();
    }

    public void setUsername(String username)
    {
        obtainInstance().setUsername(username);
    }

    public String getPassword()
    {
        return obtainInstance().getPassword();
    }

    public void setPassword(String password)
    {
        obtainInstance().setPassword(password);
    }

    public String getName()
    {
        return obtainInstance().getName();
    }

    public void setName(String name)
    {
        obtainInstance().setName(name);
    }

    public String getVAT()
    {
        return obtainInstance().getVAT();
    }

    public void setVAT(String VAT)
    {
        obtainInstance().setVAT(VAT);
    }

    public List<String> getTravels()
    {
        List<String> travels = new ArrayList<>();

        for(Travel t : obtainInstance().getTravels())
        {
            travels.add(t.getId());
        }

        return travels;
    }
}
