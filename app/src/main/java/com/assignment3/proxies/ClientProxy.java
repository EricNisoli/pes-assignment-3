package com.assignment3.proxies;

import com.assignment3.entities.Client;
import com.assignment3.entities.Travel;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

public class ClientProxy extends AbstractProxy<Client>
{
    public ClientProxy(Client instance)
    {
        super(instance);
    }

    public String getId()
    {
        return obtainInstance().getId();
    }

    public void setId(String id)
    {
        obtainInstance().setId(id);
    }

    public String getUsername()
    {
        return obtainInstance().getUsername();
    }

    public void setUsername(String username)
    {
        obtainInstance().setUsername(username);
    }

    public String getPassword()
    {
        return obtainInstance().getPassword();
    }

    public void setPassword(String password)
    {
        obtainInstance().setPassword(password);
    }

    public String getName()
    {
        return obtainInstance().getName();
    }

    public void setName(String name)
    {
        obtainInstance().setName(name);
    }

    public String getSurname()
    {
        return obtainInstance().getSurname();
    }

    public void setSurname(String surname)
    {
        obtainInstance().setSurname(surname);
    }

    public String getBirthday()
    {
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        return df.format(obtainInstance().getBirthday());
    }

    public void setBirthday(String birthday)
    {
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        try
        {
            obtainInstance().setBirthday(df.parse(birthday));
        } catch (Exception ex)
        {
            ex.printStackTrace();
        }
    }

    public List<String> getTravels()
    {
        List<String> travels = new ArrayList<>();

        for(Travel t : obtainInstance().getTravels())
        {
            travels.add(t.getId());
        }

        return travels;
    }
}
