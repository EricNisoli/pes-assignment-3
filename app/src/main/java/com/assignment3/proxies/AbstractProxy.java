package com.assignment3.proxies;

import com.assignment3.entities.AbstractEntity;

/**
 * Abstract class needed by {@link com.assignment3.controllers.ControllerBase}
 * to parse json objects. It's necessary in order to expose to the response
 * json object only certain {@code E} attributes, because Struts 2 json parser
 * uses setter methods as json object fields. It's also needed to build the
 * request {@code E} object from the body of the http request, since the json
 * parser uses getter methods to populate the object instance attributes and
 * certain json attributes may not be present in the {@code E} instance
 * (e.g. an id of a foreign entity).
 * @param <E> the {@link AbstractEntity} type
 */
public abstract class AbstractProxy<E extends AbstractEntity>
{
    private E instance;

    public AbstractProxy(E instance)
    {
        this.instance = instance;
    }

    public E obtainInstance()
    {
        return this.instance;
    }

    public boolean hasGeneratedId()
    {
        return this.instance.hasGeneratedId();
    }
}
