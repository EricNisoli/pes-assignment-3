package com.assignment3.proxies;

import com.assignment3.entities.Airplane;

public class AirplaneProxy extends AbstractProxy<Airplane>
{
    public AirplaneProxy(Airplane instance)
    {
        super(instance);
    }

    public String getId()
    {
        return obtainInstance().getId();
    }

    public String getModel()
    {
        return obtainInstance().getModel();
    }

    public String getManufacturer()
    {
        return obtainInstance().getManufacturer();
    }

    public void setId(String id)
    {
        obtainInstance().setId(id);
    }

    public void setManufacturer(String manufacturer)
    {
        obtainInstance().setManufacturer(manufacturer);
    }

    public void setModel(String model)
    {
        obtainInstance().setModel(model);
    }

}
