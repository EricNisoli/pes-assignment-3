package com.assignment3.proxies;

import com.assignment3.entities.Airport;
import com.assignment3.entities.Flight;

import java.util.ArrayList;
import java.util.List;

public class AirportProxy extends AbstractProxy<Airport>
{
    private String arrivalId;
    private String departureId;

    public AirportProxy(Airport instance)
    {
        super(instance);
    }

    public String getCode()
    {
        return obtainInstance().getId();
    }

    public void setCode(String id)
    {
        obtainInstance().setId(id);
    }

    public String getName()
    {
        return obtainInstance().getName();
    }

    public void setName(String name)
    {
        obtainInstance().setName(name);
    }

    public String getCity()
    {
        return obtainInstance().getCity();
    }

    public void setCity(String city)
    {
        obtainInstance().setCity(city);
    }

    public String getState()
    {
        return obtainInstance().getState();
    }

    public void setState(String state)
    {
        obtainInstance().setState(state);
    }

    public List<String> getDepartures()
    {
        List<String> departures = new ArrayList<>();

        for(Flight f : obtainInstance().getDepartures())
        {
            departures.add(f.getId());
        }

        return departures;
    }

    public List<String> getArrivals()
    {
        List<String> arrivals = new ArrayList<>();

        for(Flight f : obtainInstance().getArrivals())
        {
            arrivals.add(f.getId());
        }

        return arrivals;
    }

    public void setArrivalId(String arrivalId)
    {
        this.arrivalId = arrivalId;
    }

    public void addArrival(Flight f)
    {
        obtainInstance().addArrival(f);
    }

    public void setDepartureId(String departureId) { this.departureId = departureId; }

    public void setDeparture(Flight f)
    {
        obtainInstance().addDeparture(f);
    }

    public String obtArrivalId()
    {
        return arrivalId;
    }

    public String obtDepartureId()
    {
        return departureId;
    }
}
